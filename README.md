Scalfi_JCP_153_174704_2020

Contains input files and data used to generate the figures of the article:

A semiclassical Thomas–Fermi model to tune the metallicity of electrodes in molecular simulations

Laura Scalfi, Thomas Dufils, Kyle G. Reeves, Benjamin Rotenberg and Mathieu Salanne, J. Chem. Phys. 153, 174704 (2020)

https://doi.org/10.1063/5.0028232

The folder typical_input_files contains typical MetalWalls input files used to perform the simulations.

The folder DATA_FIGURES contains the processed data used to plot all the figures of the paper.

